﻿#include <iostream>
#include <string>
#include <windows.h>
#include <conio.h>
#include "Control.h"
#include "moy_h.h"

using namespace std;

const int N = 5;
string Menu[N];
int length[N];
string* MenuPointer = Menu;
char Move;
int Matrix[SizeOfArray][SizeOfArray];




int main()
{
    Menu[0] = "Generate new matrix";
    Menu[1] = "Number of items less than a given number";
    Menu[2] = "The number of non-zero rows";
    Menu[3] = "Sum of elements under the main diagonal";
    Menu[4] = "Exit";
    for (int i = 0; i < N; i++) length[i] = Menu[i].size();
    GenerateMatrix(Matrix);
    PrintMatrix(Matrix);
    Output(MenuPointer, N);
    gotoxy(length[0] + 6, positionY);
    while (true) {
        Move = _getch();
        if (Move == -32) {
            Move = _getch();
            switch (Move) {
            case 80:
                if (positionY < 24) MoveToDown(length[positionY - 18], positionY);
                break;
            case 72:
                if (positionY > 19) MoveToUp(length[positionY - 20], positionY);
                break;
            }
        }
        if (Move == 13) {
            char Enter;
            system("cls");
            switch (positionY) {
            case 19:
                gotoxy(0, 0);
                GenerateMatrix(Matrix);
                break;
            case 20:
                int number;
                cout << "Input number: ";
                cin >> number;
                SearchLessInString(Matrix, number);
                Enter = _getch();
                positionY = 19;
                break;
            case 21:
                cout << "Non-zero rows: ";
                SearchNoZeroElements(Matrix);
                Enter = _getch();
                positionY = 19;
                break;
            case 22:
                cout << "Summer of under diagonal: ";
                SummerElementsUnderMainDiagonal(Matrix);
                Enter = _getch();
                positionY = 19;
                break;
            case 23:
                return 0;
            }
            system("cls");
            PrintMatrix(Matrix);
            Output(MenuPointer, N);
            gotoxy(length[0] + 6, positionY);
        }
    }
    return 0;
}

#ifndef Control_H
#define Control_H

#include <iostream>
#include <string>
#include <windows.h>
#include <conio.h>
#include "Control.h"

using namespace std;

int positionY = 19;

void gotoxy(int xpos, int ypos)
{
    COORD scrn;

    HANDLE hOuput = GetStdHandle(STD_OUTPUT_HANDLE);

    scrn.X = xpos; scrn.Y = ypos;

    SetConsoleCursorPosition(hOuput, scrn);
}

void MoveToDown(int length, int& yPos) {
    gotoxy(length + 6, ++yPos);
}

void MoveToUp(int length, int& yPos) {
    gotoxy(length + 6, --yPos);
}

void Output(string* PointerMenu, int sizeMenu) {
    for (int i = 0; i < sizeMenu; i++) {
        gotoxy(3, positionY + i);
        cout << i + 1 << ") " << PointerMenu[i] << "\n";
    }
}

#endif // Control_H
